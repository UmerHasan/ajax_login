<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return response()->json(['msg' => 'ok']);
        } else {
            return response()->json(['msg' => 'error']);
        }
    }

    public function authenticate(Request $request) {
        

        if (Auth::attempt($credentials)) {
            Auth::login(App\User::findByEmail(request()->email));

            return json_encode(array([
                'msg' => 'ok'
            ]));
        } else {
            return json_encode(array([
                'msg' => 'Invalid Credentials'
            ]));
        }
    }

    public function showLoginForm() {
        return view('auth.login');
    }

    public function login()
    {
        
    }
}
