<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function authenticate(Request $request) {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            Auth::login(App\User::findByEmail(request()->email));

            return json_encode(array([
                'msg' => 'ok'
            ]));
        } else {
            return json_encode(array([
                'msg' => 'Invalid Credentials'
            ]));
        }
    }

    public function showLoginForm() {
        return view('auth.login');
    }

    public function login()
    {
        
    }
}
